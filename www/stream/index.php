<?php
// pass your ranges to this method and if there is a common intersecion it will
// return it or false

function checkIfOverlapped($ranges)
{
    $res = $ranges[0];

    $countRanges = count($ranges);

    for ($i = 1; $i < $countRanges; $i++) {

        $r1s = $res['start'];
        $r1e = $res['end'];

        $r2s = $ranges[$i]['start'];
        $r2e = $ranges[$i]['end'];

        if ($r1s >= $r2s && $r1s <= $r2e || $r1e >= $r2s && $r1e <= $r2e || $r2s >= $r1s && $r2s <= $r1e || $r2e >= $r1s && $r2e <= $r1e) {

            $res = array(
                'start' => $r1s > $r2s ? $r1s : $r2s,
                'end' => $r1e < $r2e ? $r1e : $r2e
            );

        } else return false;

    }

    return $res;
}
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  $sender_start = strtotime($_GET['notBefore']);
  $caller_start = strtotime($_GET['notAfter']);
  $sender_ender = strtotime($_GET['notBefore']);
  $caller_ender = strtotime($_GET['notAfter']);
  
  // example
  $ranges = array(
		  array('start' => $caller_start+(intval)($_GET['sofset']), 'end' => $caller_ender),
		  array('start' => $caller_start, 'end' => $caller_ender+(intval)($_GET['eofset'])),
		  );
  
  $caller_time = checkIfOverlapped($ranges)['start'];
  $sender_time = checkIfOverlapped($ranges)['end'];
  
  print $caller_time . "\n";
  print $sender_time . "\n";
  
  $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/gnomevoice/stream/data/' . $caller_time . ".voice", "r");
  fseek($fp,(intval)(8048*intval($_GET['sofset'])),SEEK_SET);
  header("HTTP/2.0 206 Partial Content\n");
  header("Content-Length: " . (intval)(8048*intval($_GET['eofset'])-intval(8048*$_GET['sofset'])) . "\n");  
  header("Content-Disposition: attachment; filename=\"" . $_GET['label'] . ".voice\"\n");
  header("Date: Mon, May 16, 2022 06:00:00 GMT\n");
  header("Server: perceptron.stream/2.0 (GNU/Linux)");
  header("Last-Modified: Fri, May 13, 2022 19:04:00 GMT\n");
  header("Accept-Ranges: bytes\n");
  header("Content-Range: bytes " . 8048*intval($_GET['sofset']) . "-" . 8048*intval($_GET['eofset']) . "/" . filesize($_SERVER['DOCUMENT_ROOT'] . '/3.0/stream/data/' . $caller_time . ".voice") . "\n");
  header("Content-Type: text/xml\n");
  header("CRLF\n");
  fpassthru($fp);
  fclose($fp);
} else {
?>
<?php
$uploaddir = '/var/www/html/gnomevoice/stream/data/';
$uploadfile = $uploaddir . time() . ".voice";
echo '<pre>';
if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
  echo "File is valid, and was successfully uploaded.\n";
  echo "<p><a href='https://www.gnomevoice.org/stream/data/" . basename($uploadfile) . "'>https://www.gnomevoice.org/stream/data/" . basename($uploadfile) . "</a></p>\n";
} else {
  echo "Possible file upload attack!\n";
}
// echo 'Here is some more debugging info:';
// print_r($_FILES);
print ("<table><tr><td>Download Voice from <a href='https://wiki.gnome.org/Apps/Voice'>https://wiki.gnome.org/Apps/Voice</a> from GNOME Foundation and upload \$HOME/Music/GNOME.voice from World Wide Web in the form below:<br />
<form enctype=\"multipart/form-data\" action=\"https://www.gnomevoice.org/stream/?notBefore=2022-06-01T00:00:00&notAfter=2525-05-31T23:59:59&label=GNOME&sofset=0&eofset=12\" method=\"POST\"><input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"5000000000\" /><!-- Name of input element determines name in $_FILES array --></td></tr>
<tr><td><input name=\"userfile\" type=\"file\" /><input type=\"submit\" value=\"Voicegram!\" /></form></td></tr><tr><td></td></tr></table>\n");
}
?>
