<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Voice 6.0.0 - http://www.gnomevoice.org/ - http://wiki.gnome.org/Apps/Voice</title>
  <link rel='stylesheet' id='home-css'  href='https://www.gnomevoice.org/css/main.css' type='text/css' media='all'>
    <style>
      body {
        margin-top: 0px;
        margin-bottom: 100px;
        margin-right: 150px;
        margin-left: 80px;
      }
      a {
        color:#1919e6;text-decoration:none;
      }
      a:visited {
        color:#1313a5;
      }
      img {
        border: 0;
      }
      div.img {
        border: 0;
        text-align: center;
      }
    </style>    
  </head>
  <body>
      <table>
	<tr>
	  <td>
	    <div class="img"><a href="https://gitlab.stud.idi.ntnu.no/olekaam/garagejam.git"><img height="86" src="https://www.garagejam.org/garagejam.svg" alt="GTK GarageJam (icon)"></a></div>
	  </td>
	  <td>
	    <div class="img"><a href="https://gitlab.gnome.org/olekaam/gtk-voice.git"><img height="86" src="https://www.gnomevoice.org/gnome-voice.svg" alt="GNU Network Object Model Environment Voice (icon)"></a></div>
	  </td>
	  <td>
	    <div class="img"><a href="https://gitlab.stud.idi.ntnu.no/olekaam/radio.git"><img height="86" src="https://www.gnomeradio.org/gnome-radio.svg" alt="GNU Network Object Model Environment Radio (icon)"></a></div>
	</td>
	</tr>
      </table>
    <main class="page-content" aria-label="Content">
      <div class="wrapper">
        <div class="home">
	  <h1 style="text-align: center"><a href="http://www.gnomevoice.org/src/gnome-voice-6.0.0.tar.xz">Voice 6 for GNU Network Object Model Environment</a></h1>
	  <p style="text-align: center"><a href="http://gitlab.gnome.org/olekaam/gtk-voice.git"><img src="gnome-voice.svg" width="280" height="280" alt="GNOME Voice"></a></p>
	  <p style="text-align: center"><a href="http://wiki.gnome.org/Apps/Voice">Voice</a> 6 is Public Voice Communication Software for GNU Network Object Model Environment 45.  The latest Voice 6.0.0 features Microphone/USB Recording into $HOME/Music/GNOME.ogg (on American English systems) and a continous Public Voice stream from <a href="http://api.perceptron.stream:8000/192.ogg">http://api.perceptron.stream:8000/192.ogg</a></p>
	  <p style="text-align: center">Configurable with Voicegram 1.0 XML in <b>$PREFIX/share/gnome-voice.xml</b> or <b>—-stream &lt;URI&gt;</b> and <b>—-filename &lt;FILE.voice&gt;</b> Command Line Options</p>
	  <p style="text-align: center">Records Mic into <b>$HOME/Music/GNOME.ogg</b> and <b>$HOME/Desktop/{$USERNAME}.voice</b></p>
<!--
	  <p style="text-align: center">Default: Free Voice broadcast of Avatars Of Love (2022) with <a href="https://www.sondrelerche.com/">Sondre Lerche</a> from Los Angeles, California on <b>api.perceptron.stream:8000/192.ogg</b></p>
-->
	  <h3 style="text-align: center"><a href="http://www.gnomevoice.org/src/gnome-voice-6.0.0.tar.xz">Download</a> | <a href="http://www.gnomevoice.org/#News">News</a> | <a href="http://www.gnomevoice.org/#Publications">Publications</a> | <a href="http://www.gnomevoice.org/#References">References</a> | <a href="http://www.gnomevoice.org/#SourceCode">Source Code</a> | <a href="http://wiki.gnome.org/Apps/Voice">Wiki</a> | <a href="http://www.gnomevoice.org/voice/?notBefore=2023-12-26T10:00:00&notAfter=2525-05-31T23:59:59&label=GNOME&sofset=0&eofset=12">Upload</a></h3>
	  <p style="text-align: center"><a href="voice-player-window.png"><img src="voice-player-window.png" alt="Voice 6.0.0 Player Window"></a></p>
	  <p style="text-align: center"><a href="voice-record-window.png"><img src="voice-record-window.png" alt="Voice 6.0.0 Record Window"></a></p>	  
	  <h2 style="text-align: center"><b><a href="http://www.gnomevoice.org/#Debian">Debian 11</a> | <a href="http://www.gnomevoice.org/#Fedora">Fedora 40</a> | <a href="http://www.gnomevoice.org/#Ubuntu">Ubuntu 22.04</a> | <a href="#macOS">macOS 14</a> | <a href="http://www.gnomevoice.org/#MacPorts">MacPorts</a></b></h2>
	  <h3 id="Debian">Installation on <a href="http://www.debian.org/">Debian 11</a> (<a href="http://forty.gnome.org/">GNU Network Object Model Environment 45</a>) from <a href="http://help.gnome.org/users/gnome-terminal/stable/">GNU Network Object Model Environment Terminal</a></h3>
	  <pre>sudo apt-get install gnome-common gcc git make wget
sudo apt-get install debhelper intltool dpkg-dev-el libgeoclue-2-dev
sudo apt-get install libgstreamer-plugins-bad1.0-dev libgeocode-glib-dev
sudo apt-get install gtk-doc-tools itstool libxml2-utils yelp-tools
sudo apt-get install libchamplain-0.12-dev libchamplain-gtk-0.12
sudo apt-get install libgstreamer-plugins-bad1.0-dev libgeocode-glib-dev
<a href="http://www.gnomevoice.org/~ole/debian/gnome-voice_6.0.0-1_amd64.deb">wget http://www.gnomevoice.org/~ole/debian/gnome-voice_6.0.0-1_amd64.deb</a><br>sudo dpkg -i gnome-voice_6.0.0-1_amd64.deb</pre>
	  <h3 id="Fedora">Installation on <a href="http://getfedora.org/">Fedora Core 40</a> (<a href="http://forty.gnome.org/">GNU Network Object Model Environment 45</a>) from <a href="http://help.gnome.org/users/gnome-terminal/stable/">GNU Network Object Model Environment Terminal</a></h3>
	  <pre><a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-6.0.0-1.fc40.x86_64.rpm">sudo dnf install http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-6.0.0-1.fc40.x86_64.rpm</a></pre>

	  <h3 id="Ubuntu">Installation on <a href="http://www.ubuntu.com">Ubuntu 22.04</a> (<a href="http://forty.gnome.org/">GNU Network Object Model Environment 45</a>) from <a href="http://help.gnome.org/users/gnome-terminal/stable/">GNU Network Object Model Environment Terminal</a></h3>
	  <pre>sudo apt-get install gnome-common gcc git make wget
sudo apt-get install debhelper intltool dpkg-dev-el libgeoclue-2-dev
sudo apt-get install libgstreamer-plugins-bad1.0-dev libgeocode-glib-dev
sudo apt-get install gtk-doc-tools itstool libxml2-utils yelp-tools
sudo apt-get install libchamplain-0.12-dev libchamplain-gtk-0.12
sudo apt-get install libgstreamer-plugins-bad1.0-dev libgeocode-glib-dev
<a href="http://www.gnomevoice.org/~ole/ubuntu/gnome-voice_6.0.0-1_amd64.deb">wget http://www.gnomevoice.org/~ole/ubuntu/gnome-voice_6.0.0-1_amd64.deb</a><br>sudo dpkg -i gnome-voice_6.0.0-1_amd64.deb</pre>
	  <h3 id="macOS">macOS 14 (Sonoma)</h3>
	  <pre><a href="http://www.gnomevoice.org/mac/gnome-voice-6.0.0_1.pkg">http://www.gnomevoice.org/mac/gnome-voice-6.0.0_1.pkg</a></pre>
	  <h3>Support Voice Development</h3>
	  <form action="http://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
	    <input type="hidden" name="cmd" value="_s-xclick">
	    <input type="hidden" name="hosted_button_id" value="5B7BHHLUHV6UY">
	    <input type="image" src="http://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button">
	    <img alt="Paypal" src="http://www.paypal.com/no_NO/i/scr/pixel.gif" width="1" height="1">
	  </form>
	  <h3 id="News">News</h3>
	  <h3>Dec 25, 2023</h3>

	  <p>Voice 6.0.0 for GNU Network Object Model Environment 45 is available <a href="http://www.gnomevoice.org/src/gnome-voice-6.0.0.tar.xz">http://www.gnomevoice.org/src/gnome-voice-6.0.0.tar.xz</a></p>

	  <p>Voice 6.0.0 for GNU Network Object Model Environment 45 is available for macOS 14 (Sonoma) <a href="http://www.gnomevoice.org/mac/gnome-voice-6.0.0_1.pkg">http://www.gnomevoice.org/mac/gnome-voice-6.0.0_1.pkg</a></p>

	  <p>Voice 6.0.0 is available in MacPorts <a href="https://ports.macports.org/port/gnome-voice">https://ports.macports.org/port/gnome-voice</a></p>

	  <pre>port install xorg-server
port install gnome-voice</pre>
	  
	  <h3>Nov 08, 2023</h3>

	  <p>Voice 5.0.0 for GNU Network Object Model Environment 45 is available <a href="http://www.gnomevoice.org/src/gnome-voice-5.0.0.tar.xz">http://www.gnomevoice.org/src/gnome-voice-5.0.0.tar.xz</a></p>

	  <p>Voice 5.0.0 for GNU Network Object Model Environment 45 is available for macOS 14 (Sonoma) <a href="http://www.gnomevoice.org/mac/gnome-voice-5.0.0.pkg">http://www.gnomevoice.org/mac/gnome-voice-5.0.0.pkg</a></p>
	  
	  <p>Voice 5.0.0 for GNU Network Object Model Environment 45 is so far only available for <a href="http://getfedora.org/">Fedora Core 39</a> from <a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-5.0.0-1.fc39.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-5.0.0-1.fc39.x86_64.rpm</a></p>

	  <h3>March 31, 2023</h3>

	  <p>Voice 1.2.0 for GNU Network Object Model Environment 44 is now available for <a href="http://getfedora.org/">Fedora Core 38</a> from <a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-1.2.0-1.fc38.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-1.2.0-1.fc38.x86_64.rpm</a></p>
	  
	  <h3>December 5, 2022</h3>

	  <p>Voice 1.2.0 for GNU Network Object Model Environment 44 is available for macOS 13 (Ventura) <a href="http://www.gnomevoice.org/mac/gnome-voice-1.2.0.dmg">http://www.gnomevoice.org/mac/gnome-voice-1.2.0.dmg</a></p>
	    
	  <h3>November 28, 2022</h3>

	  <p>Voice 1.2.0 for GNU Network Object Model Environment 43 is now available for <a href="http://getfedora.org/">Fedora Core 37</a> from <a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-1.2.0-1.fc37.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-1.2.0-1.fc37.x86_64.rpm</a></p>
	  
	  <h3>November 21, 2022</h3>

	  <p>Voice 1.0.1 for GNU Network Object Model Environment 43 is now available for <a href="http://getfedora.org/">Fedora Core 37</a> from <a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-1.0.1-1.fc37.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-1.0.1-1.fc37.x86_64.rpm</a></p>


	  <h3>November 16, 2022</h3>

	  <p>The Voice 1.0.1 stable release is available from <a href="http://download.gnome.org/sources/gnome-voice/1.1/gnome-voice-1.0.1.tar.xz">http://download.gnome.org/sources/gnome-voice/1.1/gnome-voice-1.0.1.tar.xz</a></p>

	  <h3>November 15, 2022</h3>

	  <p>The Voice 0.8.0 stable release is available from http://download.gnome.org/sources/gnome-voice/0.8/gnome-voice-0.8.0.tar.xz</p>

	  <h3>November 14, 2022</h3>

	  <p>The Voice 0.7.0 release features --filename and --stream Command Line Options and is available from http://download.gnome.org/sources/gnome-voice/0.7/gnome-voice-0.7.0.tar.xz</p>
	  
	  <h3>November 6, 2022</h3>

	  <p>The Voice 0.6.0 release features Multiple &lt;stream&gt; Player Support in $PREFIX/share/gnome-voice/gnome-voice.xml and is available from http://download.gnome.org/sources/gnome-voice/0.6/gnome-voice-0.6.0.tar.xz</p>

	  <h3>October 26, 2022</h3>

	  <p>The Voice 0.5.0 release features XML parsing in $HOME/Desktop/{$USERNAME}.voice and is available from http://download.gnome.org/sources/gnome-voice/0.5/gnome-voice-0.5.0.tar.xz</p>

	  <p>Voice 0.5.0 for GNU Network Object Model Environment 43 is now available for <a href="http://getfedora.org/">Fedora Core 37</a> from <a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-0.5.0-1.fc37.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-0.5.0-1.fc37.x86_64.rpm</a></p>

	  <h3>October 5, 2022</h3>

	  <p>The Voice 0.4.0 release features Location Tags in $HOME/Music/GNOME.voice and is available from http://download.gnome.org/sources/gnome-voice/0.4/gnome-voice-0.4.0.tar.xz</p>

	  <p>Voice 0.4.0 for GNU Network Object Model Environment 43 is now available for <a href="http://getfedora.org/">Fedora Core 37</a> from <a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-0.4.0-1.fc37.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-0.4.0-1.fc37.x86_64.rpm</a></p>
	  
	  <h3>September 25, 2022</h3>

	  <p>The first Voice 0.3.0 release with experimential wizard, XML file support in $HOME/Music/GNOME.voice and live microphone recording into $HOME/Music/GNOME.ogg is available from http://download.gnome.org/sources/gnome-voice/0.3/gnome-voice-0.3.0.tar.xz</p>
	  
	  <p>The latest Voicegram recording gets stored in $HOME/Music/GNOME.ogg and the XML meta file in $HOME/Music/GNOME.voice</p>

	  <p>You can download http://www.gnomevoice.org/src/gnome-voice-0.3.0.tar.xz and extract gnome-voice-0.3.0.tar.xz with tar.</p>

	  <p>You can compile with gcc, install and run gnome-voice as given in the INSTALL instructions on GNU systems.</p>

	  <p>You can exit gnome-voice by clicking on "Cancel".</p>

	  <p>You can upload $HOME/Desktop/{$USERNAME}.voice (or another audio recording) on http://www.gnomevoice.org/voice/</p>
	  
	  <p>You can share it with a peer who downloads for example http://www.gnomevoice.org/stream/data/1664076856.voice</p>
	  
	  <p>The number in the URI file name 1664076856.voice is Unix time, the number of seconds since epoch (January 1, 1970) since your upload of your local ~/Desktop/GNOME.ogg recording with gnome-voice 0.3.0.</p>

	  <p>More information about Voice is available on http://wiki.gnome.org/Apps/Voice and http://www.gnomevoice.org/</p>
	  
	  <h3>September 21, 2022</h3>
          <p>Voice 0.3.0 for GNU Network Object Model Environment 43 is now available for <a href="http://getfedora.org/">Fedora Core 37</a> from <a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-0.3.0-1.fc37.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-0.3.0-1.fc37.x86_64.rpm</a></p>
	  <p>Voice 0.3.0 for GNU Network Object Model Environment 43 is available from <a href="http://download.gnome.org/sources/gnome-voice/0.3/gnome-voice-0.3.0.tar.xz">http://download.gnome.org/sources/gnome-voice/0.3/gnome-voice-0.3.0.tar.xz</a> and <a href="http://wiki.gnome.org/Apps/Voice">http://wiki.gnome.org/Apps/Voice</a></p>
          <p>Latest Information about Voice is available on <a href="http://wiki.gnome.org/Apps/Voice">http://wiki.gnome.org/Apps/Voice</a> and <a href="http://www.gnomevoice.org/news">http://www.gnomevoice.org</a></p>	  
	  <h3>August 29, 2022</h3>
          <p>Voice 0.2.0 for GNU Network Object Model Environment 43 is now available for <a href="http://getfedora.org/">Fedora Core 36</a> from <a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-0.2.0-1.fc36.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-0.2.0-1.fc36.x86_64.rpm</a></p>
	  <p>Voice 0.2.0 for GNU Network Object Model Environment 43 is available from <a href="http://download.gnome.org/sources/gnome-voice/0.2/gnome-voice-0.2.0.tar.xz">http://download.gnome.org/sources/gnome-voice/0.2/gnome-voice-0.2.0.tar.xz</a> and <a href="http://wiki.gnome.org/Apps/Voice">http://wiki.gnome.org/Apps/Voice</a></p>
          <p>Latest Information about Voice is available on <a href="http://wiki.gnome.org/Apps/Voice">http://wiki.gnome.org/Apps/Voice</a> and <a href="http://www.gnomevoice.org/news">http://www.gnomevoice.org</a></p>
          <h3 id="SourceCode">Source Code</h3>
	  <p>There is available source code in the Voice repository on <a href="https://gitlab.gnome.org/olekaam/gtk-voice">https://gitlab.gnome.org/olekaam/gtk-voice</a> and <a href="http://www.gnomevoice.org/src/gnome-voice-6.0.0.tar.xz">http://www.gnomevoice.org/src/gnome-voice-6.0.0.tar.xz</a> is the Voice 6 release (Los Angeles, California).</p>
	  <pre>
git clone https://gitlab.stud.ntnu.no/olekaam/voice.git
cd voice/
autoreconf
aclocal
automake --add-missing
./configure
make
sudo make install
gnome-voice</pre>
	  <p><a href="http://www.macports.org/">Voice is available in MacPorts</a></p>                                                   
          <pre>
sudo port install xorg-server
sudo port install gnome-voice
gnome-voice</pre>
	  <h3>Fedora Core 39</h3>
          <p>You can build <a href="http://www.gnomevoice.org/src/gnome-voice-6.0.0.tar.xz">Voice 6.0.0</a> on <a href="http://www.fedoraproject.org/">Fedora Core 39</a> with the following package dependencies installed:</p>
          <pre>
sudo dnf install gnome-common
sudo dnf install intltool libtool gtk-doc geoclue2-devel yelp-tools
sudo dnf install gstreamer1-plugins-bad-free-devel geocode-glib-devel
sudo dnf install libchamplain-devel libchamplain-gtk libchamplain geoclue2
</pre>
          <h3>Debian/Ubuntu</h3>
	  <p>You can build <a href="http://www.gnomevoice.org/src/gnome-voice-6.0.0.tar.xz">Voice 6.0.0</a> on <a href="http://www.debian.org/">Debian 11</a> and <a href="http://www.ubuntu.com/">Ubuntu Linux 22.04</a> with the following package dependencies installed:</p>
          <pre>
sudo apt-get install gnome-common gcc git make wget
sudo apt-get install debhelper intltool dpkg-dev-el libgeoclue-2-dev
sudo apt-get install libgstreamer-plugins-bad1.0-dev libgeocode-glib-dev
sudo apt-get install gtk-doc-tools itstool libxml2-utils yelp-tools
sudo apt-get install libchamplain-0.12-dev libchamplain-gtk-0.12
sudo apt-get install libgstreamer-plugins-bad1.0-dev libgeocode-glib-dev
</pre>
	  <p><a href="http://wiki.gnome.org/Apps/Voice">Voice</a> is Public Voice Communication Software for GNU Network Object Model Environment 45.  The latest Voice 6 features recording into $HOME/Music/GNOME.ogg (on American English systems) and a continous public voice stream from <a href="http://api.perceptron.stream:8000/192.ogg">http://api.perceptron.stream:8000/192.ogg</a></p>
	  <pre>
git clone http://gitlab.gnome.org/GNOME/gnome-common
cd gnome-common
./autogen.sh
./configure
make
sudo make install</pre>

	  <pre>
git clone https://gitlab.gnome.org/olekaam/gtk-voice
cd voice/
autoreconf
aclocal
automake --add-missing
./configure
make
sudo make install
gnome-voice
</pre>
	  <h3 id="MacPorts">MacPorts</h3>
	  <p><a href="http://www.macports.org/">Voice is available in MacPorts</a></p>
	  <pre>
port install xorg-server
port install gnome-voice
gnome-voice</pre>
	  <h3>Distribution Packages</h3>
	  <p><a href="http://www.debian.org/">Debian GNU/Linux</a>, <a href="http://getfedora.org/">Fedora</a> and <a href="http://www.ubuntu.com/">Ubuntu</a> software installation packages of Voice for the computer hardware architectures i386, x86_64 and amd64 are now available for download and installation with the package management system tools rpm and dpkg.</p>
          <h3>Debian GNU/Linux 11 amd64</h3>
	  <p><a href="http://www.gnomevoice.org/~ole/debian/gnome-voice_2.0.0-1_amd64.deb">http://www.gnomevoice.org/~ole/debian/gnome-voice_2.0.0-1_amd64.deb</a></p>
          <h3>Fedora Core 40 x86_64</h3>
          <p><a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-6.0.0-1.fc40.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-6.0.0-1.fc40.x86_64.rpm</a></p>
	  <h3>Ubuntu 22.04</h3>
	  <p><a href="http://www.gnomevoice.org/~ole/ubuntu/gnome-voice_2.0.0-1_amd64.deb">http://www.gnomevoice.org/~ole/ubuntu/gnome-voice_2.0.0-1_amd64.deb</a></p>
	  <h3>MacPorts</h3>
<p><a href="http://www.macports.org/ports.php?by=name&substr=gnome-voice">MacPorts</a></p>
	  <h3>Installation</h3>
	  <h3>GNU/Linux</h3>
          <h4>Debian GNU/Linux 11 amd64</h4>	  
	  <pre>wget <a href="http://www.gnomevoice.org/~ole/debian/gnome-voice_2.0.0-1_amd64.deb">http://www.gnomevoice.org/~ole/debian/gnome-voice_2.0.0-1_amd64.deb</a>
sudo dpkg -i gnome-voice_2.0.0-1_amd64.deb</pre>
	  <h4>Fedora Core 40 x86_64</h4>
          <pre><a href="http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-6.0.0-1.fc40.x86_64.rpm">http://www.gnomevoice.org/~ole/fedora/RPMS/x86_64/gnome-voice-6.0.0-1.fc40.x86_64.rpm</a></pre>
	  <h4>Ubuntu 22.04</h4>
	  <pre>wget <a href="http://www.gnomevoice.org/~ole/ubuntu/gnome-voice_2.0.0-1_amd64.deb">http://www.gnomevoice.org/~ole/ubuntu/gnome-voice_2.0.0-1_amd64.deb</a>
sudo dpkg -i gnome-voice_2.0.0-1_amd64.deb</pre>
	  <h3>Mac OS</h3>
	  <h4><a href="http://www.macports.org/">MacPorts</a></h4>
	  <pre>port install xorg-server
port install <a href="http://www.macports.org/ports.php?by=name&substr=gnome-voice">gnome-voice</a></pre>
	  <h3 id="References">References</h3>
            <p><a href="http://www.gnomevoice.org/Licklider_Taylor_The-Computer-As-A-Communications-Device.pdf">Licklider/Taylor: The Computer As A Communication Device (1968)</a></p>
	  <p>Copyright &copy; 2023 <a href="https://www.aamot.engineering/">Aamot Engineering</a></p>
	  <address>Last updated 2023-12-26T10:00:00Z-02:00 by <a href="http://www.oleaamot.com/">Ole Aamot</a> &lt;ole@aamot.org&gt;</address>
	</div>
      </div>
    </main>
  </body>
</html>
