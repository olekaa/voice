Format: 3.0 (quilt)
Source: gnome-voice
Binary: gnome-voice
Architecture: any
Version: 6.0.0-1
Maintainer: Ole Aamot <ole@src.gnome.org>
Homepage: https://wiki.gnome.org/Apps/Voice
Standards-Version: 3.9.6
Build-Depends: debhelper (>> 9.0.0), pkg-config (>> 0.28-0), libgtk-3-dev (>> 3.14.5-1), libxml2-dev (>> 2.9.4), intltool (>> 0.50.2), libpango-1.0-0 (>> 1.0), libgtk-3-0 (>> 3.0), libchamplain-gtk-0.12-dev (>> 0.12.4), libgstreamer-plugins-bad1.0-dev (>> 1.14.4-1), libgeocode-glib-dev (>> 3.20), libgeoclue-2-0 (>> 2.5.2), geoclue-2.0 (>> 2.5.2), gtk-doc-tools (>> 1.25-5), itstool (>> 2.0.2-2), libxml2-utils (>> 2.9.3+dfsg1-7+deb10u1)
Package-List:
 gnome-voice deb gnome optional arch=any
Checksums-Sha1:
 36897cfc46add44a37f2f6d0ce23db42636091f7 111396 gnome-voice_6.0.0.orig.tar.xz
 791aa436dcbb808488152f2bb602e8e9a1a361da 4492 gnome-voice_6.0.0-1.debian.tar.xz
Checksums-Sha256:
 d12f41b651414b342275723594ef9fc7d8fcaa3bb1158c36dedc62b2252bfb3c 111396 gnome-voice_6.0.0.orig.tar.xz
 05c96522c62ae8456162356bd6af316f213ac49724638086aec7b6705faf373f 4492 gnome-voice_6.0.0-1.debian.tar.xz
Files:
 5f1431532511ef86c90101945f18e1da 111396 gnome-voice_6.0.0.orig.tar.xz
 a70283463734d82fdd4382b8c949a372 4492 gnome-voice_6.0.0-1.debian.tar.xz
