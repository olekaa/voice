Format: 3.0 (quilt)
Source: gnome-voice
Binary: gnome-voice
Architecture: any
Version: 4.0.0-1
Maintainer: Ole Aamot <ole@src.gnome.org>
Homepage: https://wiki.gnome.org/Apps/Voice
Standards-Version: 3.9.6
Build-Depends: debhelper (>> 9.0.0), pkg-config (>> 0.28-0), libgtk-3-dev (>> 3.14.5-1), libxml2-dev (>> 2.9.4), intltool (>> 0.50.2), libpango-1.0-0 (>> 1.0), libgtk-3-0 (>> 3.0), libchamplain-gtk-0.12-dev (>> 0.12.4), libgstreamer-plugins-bad1.0-dev (>> 1.14.4-1), libgeocode-glib-dev (>> 3.20), libgeoclue-2-0 (>> 2.5.2), geoclue-2.0 (>> 2.5.2), gtk-doc-tools (>> 1.25-5), itstool (>> 2.0.2-2), libxml2-utils (>> 2.9.3+dfsg1-7+deb10u1)
Package-List:
 gnome-voice deb gnome optional arch=any
Checksums-Sha1:
 da6d884053060637a0e21c5f90dc4dd75aadcee6 110892 gnome-voice_4.0.0.orig.tar.xz
 1d14980683f80fa834b6aaa11b0ca570ef4c3d83 3876 gnome-voice_4.0.0-1.debian.tar.xz
Checksums-Sha256:
 14841d0da3537773acf531237e86e9e3f707116d63ee3c31df1713050c307ab6 110892 gnome-voice_4.0.0.orig.tar.xz
 c3b0d0b7822060cdaac57e8355d802416cee61e3f9a58ee34c7fe6a98f2819a0 3876 gnome-voice_4.0.0-1.debian.tar.xz
Files:
 f5f8bd04ceaa19ff0ddd1820ba4cf810 110892 gnome-voice_4.0.0.orig.tar.xz
 495df17f8d00651aca9a2347072ed45f 3876 gnome-voice_4.0.0-1.debian.tar.xz
