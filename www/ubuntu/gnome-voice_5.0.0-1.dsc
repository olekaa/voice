Format: 3.0 (quilt)
Source: gnome-voice
Binary: gnome-voice
Architecture: any
Version: 5.0.0-1
Maintainer: Ole Aamot <ole@src.gnome.org>
Homepage: https://wiki.gnome.org/Apps/Voice
Standards-Version: 3.9.6
Build-Depends: debhelper (>> 9.0.0), pkg-config (>> 0.28-0), libgtk-3-dev (>> 3.14.5-1), libxml2-dev (>> 2.9.4), intltool (>> 0.50.2), libpango-1.0-0 (>> 1.0), libgtk-3-0 (>> 3.0), libchamplain-gtk-0.12-dev (>> 0.12.4), libgstreamer-plugins-bad1.0-dev (>> 1.14.4-1), libgeocode-glib-dev (>> 3.20), libgeoclue-2-0 (>> 2.5.2), geoclue-2.0 (>> 2.5.2), gtk-doc-tools (>> 1.25-5), itstool (>> 2.0.2-2), libxml2-utils (>> 2.9.3+dfsg1-7+deb10u1)
Package-List:
 gnome-voice deb gnome optional arch=any
Checksums-Sha1:
 aaab03a17eda2829e15b08a892aa8cdd213ad23b 110888 gnome-voice_5.0.0.orig.tar.xz
 cd8ccbc598c99a136e80554299b08b933b0dd936 3876 gnome-voice_5.0.0-1.debian.tar.xz
Checksums-Sha256:
 bdb24a67ec91911d2a1ca78db72676075228b3cb6332668f0a744aedcf6419cc 110888 gnome-voice_5.0.0.orig.tar.xz
 17440261f53b633013d59a87f25f670eb9a936017c90f9211b5e53e487ef55a1 3876 gnome-voice_5.0.0-1.debian.tar.xz
Files:
 a5cddcddbbf0c98d736dd1ffedbb5456 110888 gnome-voice_5.0.0.orig.tar.xz
 fd0f9c447378866317952355872099c5 3876 gnome-voice_5.0.0-1.debian.tar.xz
