var annotated_dup =
[
    [ "_LocationInfo", "struct___location_info.html", "struct___location_info" ],
    [ "_StreamInfo", "struct___stream_info.html", "struct___stream_info" ],
    [ "_VoiceInfo", "struct___voice_info.html", "struct___voice_info" ],
    [ "_VoiceOscilloscope", "struct___voice_oscilloscope.html", "struct___voice_oscilloscope" ],
    [ "_VoiceWindow", "struct___voice_window.html", "struct___voice_window" ],
    [ "_VOSCWindow", "struct___v_o_s_c_window.html", "struct___v_o_s_c_window" ],
    [ "GetVoicegramData", "struct_get_voicegram_data.html", "struct_get_voicegram_data" ],
    [ "GpsCallbackData", "struct_gps_callback_data.html", "struct_gps_callback_data" ],
    [ "GVoiceCfg", "struct_g_voice_cfg.html", "struct_g_voice_cfg" ],
    [ "LocationCallbackData", "struct_location_callback_data.html", "struct_location_callback_data" ],
    [ "OscilloscopeCallbackData", "struct_oscilloscope_callback_data.html", "struct_oscilloscope_callback_data" ],
    [ "PageInfo", "struct_page_info.html", "struct_page_info" ]
];